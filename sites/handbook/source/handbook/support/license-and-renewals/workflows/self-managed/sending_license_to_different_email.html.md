---
layout: markdown_page
title: Sending licenses to a different email address
description: "Information on the process for sending licenses to a different email address"
category: GitLab Self-Managed licenses
---

{:.no_toc}

----

## Overview

We currently do not send paid licenses out to any email other than the one used for purchasing (and within customers.gitlab.com). To have it sent to a different email address, we would need to follow the process outlined at [Associating purchases with additional accounts](/handbook/support/license-and-renewals/workflows/customersdot/associating_purchases.html).

Once the account is changed, the license can be re-sent (or obtained by the new account owner).

An exemption can be made for a temporary license to be sent to a different email address. To make such a request the customer's TAM or Account Manager should submit a [internal license request](https://gitlab-com.gitlab.io/support/internal-requests-form/)

**Note**

We cannot bypass doing this based on internal requests. It must be done via a customer submitted ticket.


Once we have received the requested proof, we can proceed to forward or resend the license.

- Find the license by following steps 1 and 2 mentioned in the [above section](#overview). 
- To resend the license to the same user, click the `Resend to customer via email` button on the right. (Looks like an envelope icon)
- To forward the license to a different address using the `Forward license email` function. 
    1. Navigate to the `Forward license email` tab. 
    1. Enter the `Destination email address`. **NOTE** It is currently not possible to copy (cc) or send to multiple contacts at once.
    1. Click the **Forward** button.
